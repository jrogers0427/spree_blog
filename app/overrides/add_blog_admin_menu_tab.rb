Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "blog_admin_tabs",
                     :insert_bottom => "#main-sidebar",
                     :text => "
                     <% if Spree.user_class && can?(:admin, Spree::BlogEntry) %>
                      <ul class='nav nav-sidebar'>
                        <li class='sidebar-menu-item'>
                          <a class='icon-link with-tip action-comment' href='/admin/blog_entries' data-original-title='' title=''>
                            <span class='icon icon-comment'></span>Blog
                          </a>
                        </li>
                      </ul>
                     <% end %>",
                     :disabled => false)
